package models;

/**
 * Created by danlakss on 12/10/15.
 */
public enum VehicleType {
    CAR, TAXI, BUS, FOREIGN
}
