package models;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by danlakss on 12/10/15.
 */
public class Shift {
    private String name, location;
    private Date date;public Date getDate(){return date;}
    private int id; public int getId(){return this.id;}
    public String getName(){return this.name;}
    public String getLocation(){return this.location;}
    private ArrayList<Entry> entryList;
    private Entry currentEntry;public Entry getCurrentEntry(){return currentEntry;}

    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss");public SimpleDateFormat getDateFormat(){return dateFormat;}

    public ArrayList<Entry> getEntryList() {
        return entryList;
    }

    //Constructor
    public Shift(String name, String location){

        this.entryList = new ArrayList<Entry>();
        this.location = location;
        this.name = name;
        this.date = new Date();


    }
    public void addNewEntry(Entry entry){
        this.entryList.add(entry);
        this.currentEntry = entry;
    }

    //Useful methods
    public void printAllEntries(){
        for (Entry e:this.getEntryList()
             ) {
            Log.d("shift","plate nr: "+e.getPlateNr() +" " +"plate type: "+e.getVehicleType());
        }
    }
}
