package models;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by danlakss on 12/10/15.
 */

public class Entry {
    private String plateNr;
    private VehicleType vehicleType;

    public VehicleType getVehicleType(){
        return this.vehicleType;
    }
    public String getPlateNr(){
        return this.plateNr;
    }
    private Date date;public Date getDate(){return date;}
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");public SimpleDateFormat getDateFormat(){return dateFormat;}
    private String dateAsString;public String getDateAsString(){return dateAsString;}

    //constructor
    public Entry(String plateNr, VehicleType type){
        this.plateNr = plateNr;
        this.vehicleType = type;
        this.date = new Date();
        dateAsString = dateFormat.format(date);
    }


}
