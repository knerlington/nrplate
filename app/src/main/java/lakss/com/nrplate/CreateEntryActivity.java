package lakss.com.nrplate;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import helpers.DeviceHelper;
import helpers.ShiftManager;
import models.Entry;
import models.VehicleType;

public class CreateEntryActivity extends AppCompatActivity {
    private ShiftManager manager;
    private RelativeLayout rootLayout, entryPopup;
    private DeviceHelper dh;
    private boolean isCancelled;
    private boolean alertVisible = false, willRotate = false;
    private String vTypeString;
    private EditText editText_plateNr;
    private TextView labelPlate, labelType;
    private AlertDialog dialog;
    static boolean showAlert = false;

    @Override
    public void onBackPressed(){
        //Do nothing
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("entry", "onCreate");
        //Set screen orientation
        if(getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        DeviceHelper.isTabletDevice(this);
        setContentView(R.layout.activity_create_entry);


        //view references
        entryPopup = (RelativeLayout)findViewById(R.id.include);
        this.editText_plateNr = (EditText)findViewById(R.id.editText_plateNr);
        this.labelPlate = (TextView) findViewById(R.id.label_plateNr);
        this.labelType= (TextView) findViewById(R.id.label_type);

        //shared preferences
        SharedPreferences sharedPrefs = this.getPreferences(Context.MODE_PRIVATE);
        this.labelPlate.setText(sharedPrefs.getString("plateNr", "bad value found").toString());
        this.labelType.setText(sharedPrefs.getString("type", "bad value found").toString());




        //ShiftManager
        this.manager = ShiftManager.getInstance();

        //entry notification box - pre code change for 7" tablet

        if(manager.getCurrentShift().getCurrentEntry() != null){
            entryPopup.setVisibility(View.VISIBLE);
        }else{
            entryPopup.setVisibility(View.INVISIBLE);
        }

        //popup fix that worked on 10" tablet

       /* this.entryPopup.setVisibility(View.INVISIBLE);*/

    }
    @Override
    public void onConfigurationChanged (Configuration newConfig){
        super.onConfigurationChanged(newConfig);
        Log.d("entry", "onConfigurationChanged");
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_create_entry);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_create_entry);
        }

        //view references
        this.entryPopup = (RelativeLayout)findViewById(R.id.include);
        this.editText_plateNr = (EditText)findViewById(R.id.editText_plateNr);
        this.labelPlate = (TextView) findViewById(R.id.label_plateNr);
        this.labelType= (TextView) findViewById(R.id.label_type);

        //shared preferences
        SharedPreferences sharedPrefs = this.getPreferences(Context.MODE_PRIVATE);
        this.labelPlate.setText(sharedPrefs.getString("plateNr", "bad value found").toString());
        this.labelType.setText(sharedPrefs.getString("type", "bad value found").toString());

        this.entryPopup.setVisibility(View.INVISIBLE);

        if(this.entryPopup.getVisibility() != View.VISIBLE && manager.getCurrentShift().getCurrentEntry() !=null && !isCancelled){
            this.entryPopup.setVisibility(View.VISIBLE);
        }

    }

    public void regret(View v){
        isCancelled = true;
        this.entryPopup.setVisibility(View.INVISIBLE);
        manager.writeEntryToCurrentShift(isCancelled);
    }
    public void createEntry(View v){
        isCancelled = false;
        VehicleType vehicleType;


        String plateNrString = editText_plateNr.getText().toString();


        Button buttonRegret = (Button)findViewById(R.id.button_regret);
        labelPlate.setText(plateNrString);

        TextView tv = new TextView(this);
        tv.setText(editText_plateNr.getText().toString());
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();




        if(!editText_plateNr.getText().toString().isEmpty()){
            editText_plateNr.setText("");
            entryPopup.setVisibility(entryPopup.VISIBLE);
            switch (v.getId()){
                case R.id.button_type_bus:
                    //create a bus type entry
                    vehicleType = VehicleType.BUS;

                    manager.getCurrentShift().addNewEntry(new Entry(plateNrString, vehicleType));

                    manager.writeEntryToCurrentShift(isCancelled);
                    vTypeString = getString(R.string.vehicleType_bus);
                    labelType.setText(vTypeString);

                    editor.putString("type", vTypeString);
                    editor.putString("plateNr", plateNrString);
                    editor.commit();
                    break;
                case R.id.button_type_car:
                    //create a car type entry
                    vehicleType = VehicleType.CAR;
                    manager.getCurrentShift().addNewEntry(new Entry(plateNrString, vehicleType));
                    manager.writeEntryToCurrentShift(isCancelled);
                    vTypeString = getString(R.string.vehicleType_car);
                    labelType.setText(vTypeString);
                    editor.putString("type", vTypeString);
                    editor.putString("plateNr", plateNrString);
                    editor.commit();
                    break;
                case R.id.button_type_foreign:
                    //create a foreign type entry
                    vehicleType = VehicleType.FOREIGN;
                    manager.getCurrentShift().addNewEntry(new Entry(plateNrString, vehicleType));
                    manager.writeEntryToCurrentShift(isCancelled);
                    vTypeString = getString(R.string.vehicleType_foreign);
                    labelType.setText(vTypeString);
                    editor.putString("type", vTypeString);
                    editor.putString("plateNr", plateNrString);
                    editor.commit();
                    break;
                case R.id.button_type_taxi:
                    //create a taxi type entry
                    vehicleType = VehicleType.TAXI;
                    manager.getCurrentShift().addNewEntry(new Entry(plateNrString, vehicleType));
                    manager.writeEntryToCurrentShift(isCancelled);
                    vTypeString = getString(R.string.vehicleType_taxi);
                    labelType.setText(vTypeString);
                    editor.putString("type", vTypeString);
                    editor.putString("plateNr", plateNrString);
                    editor.commit();
                    break;
                default:
                    break;
            }
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.entry_activity_actions, menu);
        return true;
    }
/*    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.d("Entry", "onSaveInstaneState");

        if(alertVisible){
            savedInstanceState.putBoolean("showAlert", true);
            Log.d("Entry", "showAlert set as TRUE");
        }else{
            savedInstanceState.putBoolean("showAlert", false);
            Log.d("Entry", "showAlert set as FALSE");
        }

    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d("Entry", "onRestoreInstanceState");
        if(savedInstanceState.getBoolean("showAlert", false)){
            this.endPassAlert();
        }
    }*/

    @Override
    protected void onPause () {
        super.onPause();
        int r = willRotate ? 1 : 0;
        Log.d("Entry", String.format("onPause, willRotate: %d", r));
        if(!willRotate) {
            Log.d("Entry", "Will try and show alert...");

        }this.endPassAlert();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        switch (item.getItemId()){
            case R.id.action_endShift:
                //end shift
                this.endPassAlert();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void endPassAlert() {
        //create alert dialog
        if (dialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.alertMessage).setTitle(R.string.alertTitle);
            builder.setPositiveButton(R.string.alertOK, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    Intent intent = new Intent(CreateEntryActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });
            builder.setNegativeButton(R.string.alertCancel, null);
            dialog = builder.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
        }
        if (!dialog.isShowing()) {
            dialog.show();
        }
    }
}
