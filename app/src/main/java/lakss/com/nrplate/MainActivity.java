package lakss.com.nrplate;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import helpers.ShiftManager;
import models.Shift;

public class MainActivity extends AppCompatActivity {
    private ShiftManager manager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Determine screen size
        /*if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            Toast.makeText(this, "Large screen", Toast.LENGTH_LONG).show();
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            Toast.makeText(this, "Normal sized screen", Toast.LENGTH_LONG).show();
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            Toast.makeText(this, "Small sized screen", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(this, "Screen size is neither large, normal or small", Toast.LENGTH_LONG).show();
        }*/
        //Set screen orientation
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
        //ShiftManager
        this.manager = ShiftManager.getInstance();
        Log.d("fshelper", this.manager.fsHelper.printPrimaryExtStorageDirectory());
        //create app dir if ext media is mounted with write permission
        if(this.manager.fsHelper.isExternalStorageMounted()){
            this.manager.fsHelper.createDirInPublicPrimaryExtRootStorage(this.manager.getAppDirectory());
        }
    }

    //XML onClick methods
    public void createShift(View v){

        Log.d("main", "create shift button in main activity pressed!");
        //view references
        EditText editText_name = (EditText)findViewById(R.id.editText_name),
                editText_location = (EditText)findViewById(R.id.editText_location);

        //create a new shift and start new activity - works only if user enters name and location
        if(!editText_location.getText().toString().isEmpty() && !editText_name.getText().toString().isEmpty()) {
            this.manager.addShift(new Shift(editText_name.getText().toString(), editText_location.getText().toString()));

            if(this.manager.fsHelper.isExternalStorageMounted()){

                this.manager.setCurrentFile(this.manager.fsHelper.createFileInCustomDir(this.manager.getAppDirectory(), this.manager.getFileName()));

                Intent intent = new Intent(this, CreateEntryActivity.class);
                startActivity(intent);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onRestart(){
        super.onRestart();
        //view references
        EditText editText_name = (EditText)findViewById(R.id.editText_name),
                editText_location = (EditText)findViewById(R.id.editText_location);
        editText_location.setText("");
        editText_name.setText("");
    }


}
