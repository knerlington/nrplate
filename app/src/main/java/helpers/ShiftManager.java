package helpers;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import models.Entry;
import models.Shift;

/**
 * Created by danlakss on 12/10/15.
 */
public class ShiftManager {
    //singleton specific vars and methods
    private static ShiftManager ourInstance;


    public static ShiftManager getInstance() {
        if(ourInstance == null){
            ourInstance = new ShiftManager();

        }
        return ourInstance;
    }
    //instance vars
    private ArrayList<Shift> shiftList;
    private Shift currentShift;
    private File currentFile; public File getCurrentFile(){return currentFile;}public void setCurrentFile(File file){this.currentFile = file;}
    public FSHelper fsHelper;

    private final String appDirectory = "NrPlateMainDir";public String getAppDirectory() {
        return appDirectory;
    }
    private final String fileExtension = ".csv";
    private String fileName;

    public String getFileName() {
        return fileName;
    }

    private ShiftManager() {
       this.shiftList = new ArrayList<Shift>();
        this.fsHelper = new FSHelper();


    }

    //addShift()
    //adds a shift to the list of shifts, sets current shift to the passed param
    public void addShift(Shift shift){

        this.shiftList.add(shift);
        this.currentShift = shift;
        String dateString = this.currentShift.getDateFormat().format(this.currentShift.getDate());
        Log.d("shift", dateString);
        fileName = String.format("%s%s%s%s", this.currentShift.getLocation(),"_"+this.currentShift.getName(),"_"+dateString,fileExtension);
    }

    public void writeToCurrentShift(){

        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.currentFile, true /*append*/));
            writer.write(this.currentShift.getName());
            writer.write(this.currentShift.getLocation());
            writer.write(this.currentShift.getDate().toString());
            writer.close();
        }catch(IOException e){

        }

    }
    public void writeEntryToCurrentShift(boolean isCancelled){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.currentFile, true /*append*/));



            if(!isCancelled){
                if(this.getCurrentShift().getEntryList().size() > 1){
                    writer.append("\n"); //append new line
                }
                writer.append(this.currentShift.getCurrentEntry().getDateAsString()); //write date
                writer.append(",");

                writer.append(this.currentShift.getLocation()); //write location
                writer.append(",");

                writer.append(this.currentShift.getCurrentEntry().getVehicleType().toString()); //write type
                writer.append(",");

                writer.append(this.currentShift.getCurrentEntry().getPlateNr()); //write plateNr
                writer.append(",");

                writer.append(this.currentShift.getName()); //write name
                writer.append(",");


            }else{
                writer.append("CANCELLED"); //write canceled
                writer.append(",");

            }

            writer.close();
        }catch(IOException e){

        }
    }

    public void appendCanceledEntry(){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.currentFile, true /*append*/));

            writer.append(this.currentShift.getCurrentEntry().getDateAsString()); //write date
            writer.append(",");

            writer.append(this.currentShift.getLocation()); //write location
            writer.append(",");

            writer.append(this.currentShift.getCurrentEntry().getVehicleType().toString()); //write type
            writer.append(",");

            writer.append(this.currentShift.getCurrentEntry().getPlateNr()); //write plateNr
            writer.append(",");

            writer.append(this.currentShift.getName()); //write name
            writer.append(",");

            writer.append("CANCELED"); //write canceled
            writer.append(",");

            writer.append("\n"); //append new line
            writer.close();
        }catch(IOException e){

        }
    }

    public void printAllShifts(){
        for (Shift shift:this.shiftList
             ) {
            Log.d("ShiftManager:", shift.getName() + " " +shift.getLocation());
        }
    }
    public Shift getCurrentShift(){
        return this.currentShift;
    }

}
